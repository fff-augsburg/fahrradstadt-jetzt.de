#!/usr/bin/env perl

use warnings;
use strict;

use CGI;
use POSIX qw< strftime >;

my $q = CGI->new();
print $q->redirect("/danke.html");

my @data = (scalar($q->param("contact")), scalar($q->param("count")));
die if $data[0] =~ /\t\n/;

open my $fh, ">>", "/home/iblech/fahrradstadt-jetzt.de" or die $!;
print $fh join("\t", strftime("%c", localtime), @data), "\n";
