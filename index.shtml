<!DOCTYPE html>
<html lang="de"><head>
  <title>Augsburger Radentscheid</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="initial-scale=1">
  <meta property="og:url" content="https://www.fahrradstadt-jetzt.de/">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Augsburger Radentscheid">
  <meta property="og:description" content="Ein Bürgerbegehren, das Augsburg zur Fahrradstadt machen soll.">
  <link rel="shortcut icon" type="image/svg+xml" href="images/favicon.svg">
  <meta property="og:image" content="images/Downloadbanner.png">
  <meta property="og:image:width" content="1000">
  <meta property="og:image:height" content="300">
  <link rel="stylesheet" href="layout.css">
  <style>
    .nav-home a { font-weight: bold; }
    .announcement { border: 4px solid #00adef; }
    .logos { text-align: center; }
    .logos img { vertical-align: middle; padding: 1em; }
  </style>
</head>

<body>

<!--#include file="banner.shtml"-->
<!-- <p></p>
<p style="text-align: center"><a href="https://www.fahrradstadt-jetzt.de/unterschriftenliste.pdf"><img src="images/Downloadbanner.svg" style="width: 80%" align="middle" alt="Unterschriftenliste"></a></a>
<div style="width: 80%; margin-left: auto; margin-right: auto"><p>Durch einen Klick auf die orangenen Links bekommt ihr <a href="unterschreiben">Hinweise zum Unterschreiben</a> und eine <a href="sammelstellen">Übersicht aller Sammelstellen</a> für die Abgabe.</p></div> -->

<div class="slider">
  <div style="position: absolute; z-index: 1; text-align: center; left: 0; right: 0; bottom: 1em; scroll-snap-align: center;">
    <a href="#slide-1">●</a>&nbsp;
    <a href="#slide-2">●</a>&nbsp;
    <a href="#slide-3">●</a>
  </div>

  <div class="slides">
    <div id="slide-1">
      <img src="images/photos/fal-small.jpeg" srcset="images/photos/fal.jpeg 919w, images/photos/fal-small.jpeg 368w" sizes="100vw" alt="Umweltfreundliche Mobilität und mehr Grün in der Stadt erhöhen den Lebenswert in Augsburg.">
    </div>
    <div id="slide-2">
      <img src="images/photos/adfc-small.jpeg" srcset="images/photos/adfc.jpeg 960w, images/photos/adfc-small.jpeg 384w" sizes="100vw" alt="Wir wollen, dass sich ALLE Menschen zügig, sicher und bequem mit dem Fahrrad in Augsburg bewegen können." loading="lazy">
    </div>
    <div id="slide-3">
      <img src="images/photos/fff-small.jpeg" srcset="images/photos/fff.jpeg 960w, images/photos/fff-small.jpeg 384w" sizes="100vw" alt="Motorisierter Straßenverkehr steht auf Platz 3 der größten CO₂-Verursacher Deutschlands. Die Alternativen müssen attraktiver werden!" loading="lazy">
    </div>
  </div>
</div>
<div class="announcement">
<!--  <p><strong>Radentscheid erreicht Vereinbarung mit der Stadt</strong></p>
<p>
Nach vielen Sitzungen ist es nun soweit: Initiatoren und Stadt haben sich auf einen öffentlich-rechtlichen Vertrag geeinigt. Dieser beinhaltet wesentlich mehr als was durch die knappen Forderungen des Bürgerbegehrens hätte erreicht werden können. Er umfasst 6 Regelkomplexe und über 25 Einzelmaßnahmen, die die Stadt nun in den nächsten 5 Jahren anpacken muss.</p>
<p>
Der Vertrag wurde am 5. Juli 2021 in einer gemeinsamen <a href="https://www.youtube.com/watch?v=xDnBi46dmt0 ">Pressekonferenz mit der Oberbürgermeisterin Weber</a> vorgestellt. <a href="210705_Praesentation-Pressekonferenz_Radentscheid.pdf">Die Präsentation mit der Zusammenfassung der Ergebnisse ist hier verfügbar.</a> Am 22. Juli 2021 wurde der Vertrag dann offiziell vom Stadtrat beschlossen und am selben Tag auch von den Initiatoren unterschrieben.</p>
<p>
Wichtig ist, dass wir jetzt dranbleiben und schauen, dass das, was uns im Vertrag versprochen wurde auch umgesetzt wird. Und falls nicht, Widerspruch einlegen. So wie wir es z.B. bei der Stellplatzsatzung getan haben. Die sollte im Dezember 2021 verabschiedet werden. Wird jetzt aber nochmal überarbeitet. Wir freuen uns dabei weiter über Aktive, die uns bei diesem Prozess unterstützen wollen. </p>
-->
<p><strong>1 Jahr Radentscheid-Vertrag - eine erste Bilanz</strong></p>
<p>
Im Juli 2021 hat das Aktionsbündnis „Fahrradstadt Jetzt“ einen <a href="210705_Praesentation-Pressekonferenz_Radentscheid.pdf">Vertrag</a> mit der Stadt Augsburg geschlossen. Darin wurden die Ziele des Radentscheid Augsburg übernommen und zusätzlich mehr als 30 Einzelmaßnahmen vereinbart. Nach einem Jahr ziehen wir eine gemischte Bilanz. Einerseits ist etliches angeschoben und viel erledigt worden. Andererseits schiebt die Stadtregierung die großen Themen vor sich her. Das betrifft vor allem die Stellplatzsatzung, die der Stadtrat laut Vertrag im Dezember 2021 hätte beschließen müssen. </p>
<p>Hier findet ihr die <a href="Update-1Jahr">Bilanz zum 1-jährigen Jubiläum</a> und eine <a href="http://u.osmfr.org/m/790101/">Karte mit dem Status der Einzelprojekte (überarbeitet Februar 2025)</a>.</p>
</div>
<p><a
href="https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/verkehr/radverkehr/fahrradstadt-augsburg-grundsatzbeschluss.pdf">Seit
2012 soll Augsburg Fahrradstadt werden.</a> Aber passiert ist in diesen
sieben Jahren kaum etwas: Die Ziele für die
Steigerung des Radverkehrs wurden weit verfehlt, stattdessen <a
href="https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/jahrbuch/2017Jahrbuch_Internet_neu.pdf#page=100">steigt die Zahl
verunfallter Radfahrer Jahr für Jahr</a>.
Gleichzeitig hat <a
href="https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/jahrbuch/2017Jahrbuch_Internet_neu.pdf#page=99">der Autoverkehr in Augsburg weiter
zugenommen</a>. Das zu ändern
und eine wirkliche Mobilitätswende anzustoßen
ist das Ziel dieses Radentscheids. Von März bis November 2020 haben wir 15.543 Unterschriften gesammelt. Damit haben wir die nötige Mindestzahl (Quorum) für ein Bürgerbegehren um mehr als 40% übertroffen. Aktuell befinden wir uns in Gesprächen mit der Stadtregierung, um zeitnah mit konkreten Projekten unsere Forderungen in die Realität umsetzen zu können.</p>
<main>
 
  
  <!--
  <div class="announcement">
    <p><strong>Unterschriftenstart</strong> am <strong>1. März</strong>
    (Sonntag) um 15:00 Uhr auf der <a
    href="https://www.augsburgforfuture.de/">Klimademo</a>!<br>
    Alle wichtigen Termine im Überblick:</p>
    <ul>
      <li>1.3. 15:00 Rathausplatz &ndash; <a
      href="https://www.augsburgforfuture.de/">Klimademo</a> mit Start der
      Unterschriftensammlung</li>
      <li>4.3. 19:30 Zeughaus &ndash; <a
      href="http://www.adfc-augsburg.de/index.php/component/jevents/icalrepeat.detail/2020/03/04/9070/-/infoabend-radentscheid?Itemid=1">Informationsabend</a>
      (<a href="https://www.facebook.com/events/1639852836162792/">Facebook-Event</a>)</li>
      <li>22.3. 14:30 Königsplatz &ndash; <a href="http://www.adfc-augsburg.de/index.php/222-organisation/515-1-kidical-mass-augsburg-die-bunte-fahrrad-demo-fuer-alle">Kidical Mass</a>, die bunte Fahrrad-Demo für alle (<a href="https://www.facebook.com/events/630626994422869/">Facebook-Event</a>)</li>
    </ul>
  </div>
  -->

  <h2>Unsere Ziele</h2>

  <!--#include file="ziele-kurz.shtml"-->

  <p><a href="ziele.html#details">Der genaue Wortlaut unserer Ziele
  kann hier nachgelesen werden.</a></p>


  <h2>Wer profitiert davon?</h2>

  <p>Dieser Radentscheid ist ein Bürgerbegehren, das ein Bündel sehr
  unterschiedlicher Maßnahmen umfasst. Im Ergebnis werden davon nicht nur die
  Radfahrenden, sondern <strong>alle Bürger*innen profitieren</strong>:</p>

  <p>Das Radfahren wird sicherer, Fußgänger*innen profitieren von weniger Lärm und
  Abgasen, die Innenstadtbewohner*innen von mehr Lebensqualität und die ganze
  Gesellschaft von weniger Unfällen. Entscheidend profitiert auch das Klima: <a
  href="https://www.ndr.de/ratgeber/klimawandel/CO2-Ausstoss-in-Deutschland-Sektoren,kohlendioxid146.html">Straßenverkehr
  ist Deutschlands drittgrößter CO₂-Verursacher</a>, und dieser Radentscheid
  sorgt dafür, dass sich das ändert.</p>


  <h2>Was sagt die Presse dazu?</h2>
  <div id="presse">

    <div class="two-parts">
      <div><a href="https://www.sueddeutsche.de/bayern/augsburg-verkehrswende-radverkehr-adfc-1.5342551"><img alt="" src="images/presse/sz-2.jpeg"></a></div>
      <p>
        <a href="https://www.sueddeutsche.de/bayern/augsburg-verkehrswende-radverkehr-adfc-1.5342551"><strong>Süddeutsche Zeitung</strong> (6.7.2021)<br>
        <strong>Augsburg schließt den Radlervertrag</strong><br>
        Um ein Bürgerbegehren zu vermeiden, hat die Stadt eine Vereinbarung mit diversen Initiativen geschlossen. Darin werden Maßnahmen für die nächsten Jahre festgelegt - unter anderem müssen Hunderte Parkplätze weichen.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.br.de/nachrichten/bayern/vertrag-geschlossen-augsburgs-weg-zur-fahrradstadt,ScIdmKf"><img alt="" src="images/presse/br-4.jpeg"></a></div>
      <p>
        <a href="https://www.br.de/nachrichten/bayern/vertrag-geschlossen-augsburgs-weg-zur-fahrradstadt,ScIdmKf"><strong>Bayerischer Rundfunk</strong> (5.7.2021)<br>
        <strong>Vertrag geschlossen: Augsburgs Weg zur Fahrradstadt</strong><br>
        Augsburg setzt aufs Fahrrad: Die Stadt hat mit den Initiatoren des Radentscheids einen Vertrag geschlossen, der mehr als 25 Projekte vorsieht. Um sie umzusetzen, will die Stadt den Rad-Etat um eine halbe Million Euro erhöhen und Stellen schaffen.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.br.de/nachrichten/bayern/augsburger-radentscheid-positive-zwischenbilanz,SIB6ws0"><img alt="" src="images/presse/br-3.jpeg"></a></div>
      <p>
        <a href="https://www.br.de/nachrichten/bayern/augsburger-radentscheid-positive-zwischenbilanz,SIB6ws0"><strong>Bayerischer Rundfunk</strong> (4.12.2020)<br>
        <strong>Mehr als 15.000 Unterschriften für die "Fahrradstadt Augsburg"</strong><br>
        Sieben Monate lang haben die Aktiven des Augsburger Radentscheids Unterschriften für ein Bürgerbegehren gesammelt. Sie fordern von der Stadt mehr Investitionen für den Radverkehr. Heute haben sie eine Zwischenbilanz gezogen.</a>
      </p>
    </div>
    
   <input class="more" type="checkbox" id="more-press">
    <p><label class="more" for="more-press">Weitere Artikel</label></p>
    <div class="more-content">

    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Rad-Aktivisten-machen-Druck-auf-die-Stadt-Augsburg-id58665526.html"><img alt="" src="images/presse/AZ-7.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Rad-Aktivisten-machen-Druck-auf-die-Stadt-Augsburg-id58665526.html"><strong>Augsburger Allgemeine</strong> (4.12.2020)<br>
        <strong>Fahrrad-Bürgerbegehren: Rad-Aktivisten machen Druck auf die Stadt Augsburg</strong><br>
        Die Verhandlungen zum Radbegehren mit OB Eva Weber (CSU) scheinen nicht so recht voranzugehen. Gibt es bis Februar kein Ergebnis, soll es zur Abstimmung über die Forderungen kommen.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-in-Augsburg-Was-passiert-mit-den-Unterschriften-id58269106.html"><img alt="" src="images/presse/AZ-6.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-in-Augsburg-Was-passiert-mit-den-Unterschriften-id58269106.html"><strong>Augsburger Allgemeine</strong> (7.10.2020)<br>
        <strong>Radbegehren in Augsburg: Was passiert mit den Unterschriften?</strong><br>
        Das Bürgerbegehren zur Förderung des Radverkehrs dürfte genug Unterstützer haben. Warum die Initiatoren die Listen bisher nicht bei der Stadt eingereicht haben.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.augsburg.tv/mediathek/video/augsburger-radentscheid-hat-die-noetigen-unterschriften-zusammen/"><img alt="" src="images/presse/a-tv-1.jpeg"></a></div>
      <p>
        <a href="https://www.augsburg.tv/mediathek/video/augsburger-radentscheid-hat-die-noetigen-unterschriften-zusammen/"><strong>a.tv</strong> (3.8.2020)<br>
        <strong>Augsburger Radentscheid hat genügend Unterschriften</strong><br>
        Rund 13.000 Unterschriften haben die Initiatoren des Augsburger Radentscheids im vergangenen halben Jahr gesammelt. [...] Das Interesse der Augsburger an besseren Radwegen und einem ausgebauten Radwegenetz scheint also groß zu sein und auch die neue Stadtregierung von Eva Weber mit den Grünen wollte sich genau um solche Themen kümmern. [...]</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-Eine-Chance-endlich-anzupacken-id57852411.html"><img alt="" src="images/presse/az-5.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-Eine-Chance-endlich-anzupacken-id57852411.html"><strong>Augsburger Allgemeine</strong> (2.8.2020)<br>
        <strong>Radbegehren: Eine Chance, endlich anzupacken</strong><br>
        Die Initiatoren des Radbegehrens in Augsburg haben 13.000 Unterstützer gefunden. Das wäre eine Steilvorlage, endlich groß zu denken.</a>
      </p>
    </div>
    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Fahrradbegehren-Initiatoren-wollen-bis-Herbst-genug-Unterschriften-haben-id57428471.html"><img alt="" src="images/presse/az-4.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Fahrradbegehren-Initiatoren-wollen-bis-Herbst-genug-Unterschriften-haben-id57428471.html"><strong>Augsburger Allgemeine</strong> (23.5.2020)<br>
        <strong>Fahrradbegehren: Initiatoren wollen bis Herbst genug Unterschriften haben</strong><br>
        Das Augsburger Fahrrad-Bürgerbegehren hat schon mehr als zwei Drittel der nötigen Unterschriften. Demnächst steht ein Gespräch mit der Stadtspitze an.</a>
      </p>
    </div>
<div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-Initiatoren-wollen-laenger-Unterschriften-sammeln-id57183291.html"><img alt="" src="images/presse/az-3.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Radbegehren-Initiatoren-wollen-laenger-Unterschriften-sammeln-id57183291.html"><strong>Augsburger Allgemeine</strong> (5.4.2020)<br>
        <strong>Radbegehren: Initiatoren wollen länger Unterschriften sammeln</strong><br>
        Die Aktivisten verabschieden sich von ihrem Ziel, alle nötigen Unterschriften bis zum 4. Mai einzureichen. Das liegt auch an der Corona-Krise.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Mehr-als-6000-Unterschriften-fuer-Radbegehren-in-Augsburg-id57149071.html"><img alt="" src="images/presse/az-2.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Mehr-als-6000-Unterschriften-fuer-Radbegehren-in-Augsburg-id57149071.html"><strong>Augsburger Allgemeine</strong> (30.3.2020)<br>
        <strong>Mehr als 6000 Unterschriften für Radbegehren in Augsburg</strong><br>
        Das Bürgerbegehren zur Förderung des Radverkehrs in Augsburg hat inzwischen über 6000 Unterschriften. Die Coronakrise verlangsamt die Sammlung.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.br.de/nachrichten/bayern/vorerst-letzte-fridays-for-future-demo-in-augsburg,RrzafYq"><img alt="" src="images/presse/br-2.jpeg"></a></div>
      <p>
        <a href="https://www.br.de/nachrichten/bayern/vorerst-letzte-fridays-for-future-demo-in-augsburg,RrzafYq"><strong>Bayerischer Rundfunk</strong> (1.3.2020)<br>
        <strong>Vorerst letzte Fridays-for-Future-Demo in Augsburg</strong><br>
        In der Augsburger Innenstadt haben am Sonntagnachmittag rund 2.500
        Menschen für mehr politisches Engagement gegen den Klimawandel
        demonstriert. Thema auf der Kundgebung war auch der Beginn der
        Unterschriftensammlung für das Rad-Begehren.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.augsburger-allgemeine.de/augsburg/Buergerbegehren-Radler-sammeln-bald-Unterschriften-id56837551.html"><img alt="" src="images/presse/az-2.jpeg"></a></div>
      <p>
        <a href="https://www.augsburger-allgemeine.de/augsburg/Buergerbegehren-Radler-sammeln-bald-Unterschriften-id56837551.html"><strong>Augsburger Allgemeine</strong> (22.2.2020)<br>
        <strong>Bürgerbegehren: Radler sammeln bald Unterschriften</strong><br>
        Die Initiatoren des Bürgerbegehrens stellen Forderungen zum Radverkehr vor. Sie wollen in zwei Monaten die nötige Zahl von 11.000 Unterstützern gewinnen.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.br.de/nachrichten/bayern/aktivisten-fordern-bessere-radwege-in-augsburg,Rpim9Z0"><img alt="" src="images/presse/br-1.jpeg"></a></div>
      <p>
        <a href="https://www.br.de/nachrichten/bayern/aktivisten-fordern-bessere-radwege-in-augsburg,Rpim9Z0"><strong>Bayerischer Rundfunk</strong> (6.2.2020)<br>
        <strong>Aktivisten fordern bessere Radwege in Augsburg</strong><br>
        „Fahrradstadt“ will Augsburg sein, so wurde es im Stadtrat beschlossen. Doch Anspruch und Realität klaffen laut Aktivisten weit auseinander. Noch immer gebe es für Fahrradfahrer viele Unfallschwerpunkte.</a>
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.sueddeutsche.de/bayern/augsburg-verkehr-fahrradstadt-buergerbegehren-1.4778509"><img alt="" src="images/presse/sz-1.jpeg"></a></div>
      <p>
        <a href="https://www.sueddeutsche.de/bayern/augsburg-verkehr-fahrradstadt-buergerbegehren-1.4778509"><strong>Süddeutsche Zeitung</strong> (30.1.2020)<br>
        <strong>Augsburg soll Fahrradstadt werden</strong><br>
        Initiatoren wollen ein Bürgerbegehren starten und erhoffen sich breite
        Unterstützung. Die Parteien zeigen sich offen.</a>
      </p>
    </div>

 
      <div class="two-parts">
        <div><a href="https://www.augsburger-allgemeine.de/augsburg/Rad-Aktivisten-planen-ein-Buergerbegehren-in-Augsburg-id56566326.html"><img alt="" src="images/presse/az-1.jpeg"></a></div>
        <p>
          <a href="https://www.augsburger-allgemeine.de/augsburg/Rad-Aktivisten-planen-ein-Buergerbegehren-in-Augsburg-id56566326.html"><strong>Augsburger Allgemeine</strong> (27.1.2020)<br>
          <strong>Rad-Aktivisten planen ein Bürgerbegehren in Augsburg</strong><br>
          Die Stadt hat es sich zum Ziel gesetzt, mehr für den Fahrradverkehr
          zu tun. Doch Rad-Aktivisten gehen diese Bemühungen nicht weit genug.
          Was sie vorhaben.</a>
        </p>
      </div>

      <div class="two-parts">
        <div><a href="https://www.daz-augsburg.de/radentscheid-was-die-aktivisten-wollen/"><img alt="" src="images/presse/daz-1.jpeg"></a></div>
        <p>
          <a href="https://www.daz-augsburg.de/radentscheid-was-die-aktivisten-wollen/"><strong>Die Augsburger Zeitung</strong> (30.1.2020)<br>
          <strong>Radentscheid: Was die Aktivisten wollen</strong><br>
          Nachdem die DAZ am vergangenen Samstag exklusiv über das kommende
          Bürgerbegehren „Fahrradstadt jetzt“ berichtete, hatten die Rad-Aktivisten
          wegen zahlreicher Medienanfragen kaum noch eine ruhige Minute. Nun äußern
          sie sich dazu mit einem ausführlichen Statement.</a>
        </p>
      </div>
    </div>
  </div>


  <h2>Was ist der aktuelle Stand?</h2>

  <p>Am 1. März 2020 haben wir auf dem Rathausplatz bei der großen Klima-Demo mit dem Unterschriftensammeln begonnen. Und mehr als 1000 haben gleich am selben Tag unterschrieben. Bis November haben wir gesammelt, haben mit tausenden Augsburger*innen gesprochen und sie von unseren Forderungen überzeugt. Das Ergebnis ist beeindruckend: 15.543 Augsburger*innen sagen Ja.</p>
  <ul>
    <li>Ja, für eine Fahrradstadt, die erlebbar ist.</li>
    <li>Ja, für eine Fahrradstadt, die Lebensqualität bringt.</li>
    <li>Ja, für eine Fahrradstadt, die mehr Platz schafft fürs Rad.</li>
  </ul>
  <p>Mit 15.543 Unterschriften haben wir die nötige Mindestzahl (Quorum) für ein Bürgerbegehren um über 40% übertroffen. Jetzt könnten wir unsere Kartons mit allen Unterschriften im Rathaus abgeben. Aber wir wollen noch abwarten: die Stadtregierung hat uns zu Gesprächen eingeladen. Für uns sind die Gespräche sinnvoll, weil die Forderungen im Radentscheid fast nur abstrakte Regeln enthalten aber keine konkreten Projekte. Das hat juristische Gründe, die wir akzeptieren mussten. Bei den Verhandlungen geht es darum, die praktische Umsetzung unserer Forderungen festzuklopfen. Das bedeutet, dass wir mit der Stadtregierung konkrete Baumaßnahmen festlegen und dafür sorgen, dass das dafür nötige Geld und Fachpersonal bereitsteht.</p>


  <h2>Wie kann ich helfen?</h2>
  <!--<div id="unterstuetzung">
    <div class="two-parts">
      <div><img alt="" src="images/icons/radlbotschafter.svg"></div>
      <p>
        <form action="radlbotschaft.pl" method="post">
          <label for="radlbotschafter">
            <strong class="heading">Radlbotschafter*in werden</strong><br>
            Radlbotschafter*innen sind Menschen, die in ihrem Umfeld Unterschriften für
            das Bürgerbegehren sammeln: in der Familie, im Freundeskreis oder auf
            der Arbeit. Ein paar besonders Motivierte kündigten an, 100 oder mehr
            Unterschriften sammeln zu wollen; die meisten sammeln eine große
            Handvoll. Wenn Augsburgs Radentscheid ein Erfolg wird und Augsburg
            dadurch deutlich lebenswerter wird, dann ist das zu einem erheblichen
            Teil den Radlbotschafter*innen zu verdanken.<br><br>
          </label>

          <input type="checkbox" value="radlbotschafter" id="radlbotschafter">
          <label for="radlbotschafter">
            Ja, ich möchte Radlbotschafter*in werden.<br>
          </label>

          <span style="visibility: hidden"><input type="checkbox"></span>
          Bitte schickt mir Infomaterialien zu.<br>

          <span style="visibility: hidden"><input type="checkbox"></span>
          <label for="schaetzung">
            Ich rechne damit, etwa
            <input type="number" id="schaetzung" name="count" min="1" max="10000" step="1" value="10">
            Unterschriften sammeln zu können.<br>
          </label>

          <span style="visibility: hidden"><input type="checkbox"></span>
          <label for="contact">
            Kontaktdaten:
            <input type="email" id="contact" name="contact" placeholder="Mail-Adresse"><br>
          </label>

          <span style="visibility: hidden"><input type="checkbox"></span>
          <button type="submit">Absenden</button>
        </form>
      </p>
    </div>

    <div class="two-parts">
      <div><img alt="" src="images/icons/informieren.svg"></div>
      <p>
        <strong class="heading">Informieren</strong><br>
        <a href="fragen-und-antworten.html">Häufig gestellte Fragen und
        Antworten</a>
      </p>
    </div>

    <div class="two-parts">
      <div><img alt="" src="images/icons/spenden.svg"></div>
      <p>
        <strong class="heading">Spenden</strong><br>
        Die Mitbestimmung und aktive Teilhabe von uns allen über Bürgerbegehren
        kostet Geld. <a href="spenden.html">Unterstütze die Kampagne „Fahrradstadt jetzt“ mit einer
        Spende!</a></p>
    </div>
  </div>

  <p>Du möchtest dich über das Unterschriften sammeln hinaus beim Radentscheid einbringen? Du kennst Firmen die uns
  unterstützen möchten, fotografierst gern oder hast andere künstlerische
  Talente, Social Media ist dein Zuhause und du möchtest auf unseren Kanälen
  für den Radentscheid posten? Oder vielleicht hast du auch eine ganze andere
  Idee, wo du dich einbringen möchtest? Dann <a
  href="mailto:info@adfc-augsburg.de">schreib uns eine kurze
  Nachricht an info@adfc-augsburg.de</a>.</p>-->
  <p>Wenn du dich für die Fahrradstadt in Augsburg einbringen willst, sei es in der Verkehrspolitik, auf Social Media oder bei der Vorbereitung von Aktionen, dann <a href="mailto:info@adfc-augsburg.de">schreib uns eine kurze Nachricht an info@adfc-augsburg.de</a>.</p>
  

  <h2>Träger des Bürgerbegehrens</h2>

  <p>Das Aktionsbündnis <em>Fahrradstadt jetzt</em> wird von drei Organisationen
  sowie vielen fahrradengagierten Bürger*innen getragen. Zudem gibt es ein <a
  href="netzwerk">Netzwerk aus Bündnispartnern und Unterstützern:
  Organisationen, Parteien und Unternehmen</a>.</p>

  <div id="traeger">
    <div class="two-parts">
      <div><a href="http://www.adfc-augsburg.de/"><img alt="" src="images/logos/adfc-augsburg.jpeg"></a></div>
      <p>
        <a href="http://www.adfc-augsburg.de/"><strong>Allgemeiner deutscher Fahrrad-Club ADFC</strong></a><br>
        Der ADFC ist die größte Lobbyorganisation für Radfahrer*innen in Deutschland
        mit insgesamt über 185.000 Mitgliedern.
        Deutschlandweit ist der ADFC in 450 Niederlassungen aktiv, der
        Regionalverband Augsburg und Region wird von 1600
        Mitgliedern getragen.<br>
        Kontaktperson: <a href="mailto:arne.schaeffler@adfc-augsburg.de">Arne
        Schäffler</a> (Mitglied des Vorstands)
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://forum-augsburg-lebenswert.de/"><img alt="" src="images/logos/fal.jpeg"></a></div>
      <p>
        <a href="https://forum-augsburg-lebenswert.de/"><strong>Forum Augsburg lebenswert e.V.</strong></a><br>
        Das Forum Augsburg lebenswert entstand 1987 als Zusammenschluss vieler
        Bürgerinitiativen und -aktionen und ist seit April 1990 ein
        eingetragener Verein. Es wurde gegründet, um die Lebensqualität
        insbesondere auf dem Gebiet des Umweltschutzes für die Bewohner*innen der
        Stadt Augsburg und ihres Umlandes zu erhalten und
        weiterzuentwickeln.<br>
        Kontaktperson: <a href="mailto:jens.wunderwald@posteo.de">Jens
        Wunderwald</a> (2. Vorsitzender)
      </p>
    </div>

    <div class="two-parts">
      <div><a href="https://www.fff-augsburg.de/"><img alt="" src="images/logos/fff-augsburg.svg"></a></div>
      <p>
        <a href="https://www.fff-augsburg.de/"><strong>Fridays for Future Augsburg</strong></a><br>
        Fridays for Future ist eine von Schüler*innen ausgehende weltweite soziale
        Bewegung, welche sich für möglichst umfassende, schnelle und effiziente
        Maßnahmen für Klimagerechtigkeit einsetzt, um das Pariser
        Klimaschutzabkommen noch einhalten und so die Klimaerwärmung auf
        1,5 °C begrenzen zu können. Hinter Fridays for Future Augsburg stellt
        sich ein <a href="https://www.augsburg-handelt.de/">breites Bündnis aus
        knapp 100 Augsburger Unternehmen und Institutionen</a>.<br>
        Kontaktperson: <a href="mailto:radentscheid@fff-augsburg.de">Sarah
        Bauer</a> (Pressesprecherin)
      </p>
    </div>
  </div>

  <div class="logos">
    <!--#include file="buendnispartner.shtml"-->
    <!--#include file="unterstuetzer.shtml"-->
  </div>
</main>

<!--#include file="footer.shtml"-->

</body>
</html>
