all: $(patsubst %.shtml,%.html,$(wildcard *.shtml)) $(patsubst %.shtml,%,$(wildcard *.shtml))

layouthash=$(shell git hash-object layout.css | head -c6)

layout-$(layouthash).css: layout.css
	cp $< $@

%.html: %.shtml ziele-kurz.shtml banner.shtml layout-$(layouthash).css
	perl ssi.nocgi < $< | sed -e "s+\"layout.css\"+\"layout-$(layouthash).css\"+g" > $@

%: %.html
	ln -s $< $@
